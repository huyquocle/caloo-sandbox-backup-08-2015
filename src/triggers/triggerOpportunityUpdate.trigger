trigger triggerOpportunityUpdate on Opportunity (after update) {
    	
    	Map<id,Opportunity> listOppMap = new Map<id,Opportunity>();
        for( Opportunity Newitem : trigger.new )
        {
            Opportunity oldItem =  trigger.oldMap.get(Newitem.id);
            if( oldItem.Caloo_Final_Order_Total__c != Newitem.Caloo_Final_Order_Total__c )
            {
            	if( oldItem.Has_Opportunity_Line__c == false)
            	{
	                if( oldItem.Amount == 0)
	                    continue;
            	}
                if( oldItem.Amount != newItem.Caloo_Final_Order_Total__c )
                {
                     Opportunity item = new Opportunity(id = Newitem.ID);
                     item.Amount = newItem.Caloo_Final_Order_Total__c;
                    if(listOppMap.get(item.id) == null)
                    {
                        listOppMap.put(item.id,item);
                    }
                }
            }
        }
    	if(listOppMap.size()> 0)
        	update listOppMap.values();
}