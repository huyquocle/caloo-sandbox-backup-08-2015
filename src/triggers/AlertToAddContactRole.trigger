trigger AlertToAddContactRole on Opportunity (after insert,after update) {
    if(trigger.isafter && trigger.isinsert)
    {
        Map<id,String> alertMap = new Map<id,String>();        
        List<OpportunityContactRole> oppctr = new List<OpportunityContactRole>();
        for(Opportunity opp: trigger.new)
        {
            if(opp.StageName!= 'Closed Lost' && opp.StageName!= 'Closed Won')
            {
                List<Contact> ct = opp.Account.contacts;
                if(ct!= null)
                {
                    if(ct.size() == 1)
                    {
                        OpportunityContactRole oppctrItem = new OpportunityContactRole();
                        oppctrItem.OpportunityId = opp.id;   
                        oppctrItem.ContactId = ct.get(0).id;
                        oppctrItem.IsPrimary = true;
                        oppctr.add(oppctrItem);     
                    }
                    else if( ct.size() > 1)
                    {
                        alertMap.put(opp.id,opp.id);
                    } 
                }
            }            
        }    
        if(oppctr.size()> 0)
        {
            insert oppctr;
        }        
        if(alertMap.size() > 0)
        {
            List<Task> tasks = new List<Task>();
            for(id opp:alertMap.keySet())
            {
                tasks.add(new Task(
                    ActivityDate = Date.today(),
                    Subject='Update Contact Role for Opportunity',
                    WhatId = opp,
                    OwnerId = UserInfo.getUserId(),
                    Status='In Progress'));
            }
            insert tasks;            
        }
    } 
}