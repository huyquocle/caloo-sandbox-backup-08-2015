trigger beforeInsUpdateOwner on Opportunity (before insert) {
    if(trigger.isbefore && trigger.isinsert)
    {
        for(Opportunity item: trigger.new)
        {
            if(item.account.ownerid != null)
            {
                item.ownerid = item.account.ownerid;    
            }            
        }        
    }
}