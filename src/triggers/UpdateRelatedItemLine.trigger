trigger UpdateRelatedItemLine on PBSI__PBSI_Item__c (after update) {
 
    set<id> itemids= new set<id>();    
    list<PBSI__BOM_Line__c> lines= [select id from PBSI__BOM_Line__c where PBSI__Item__c in:itemids];
    update  lines;
    
}