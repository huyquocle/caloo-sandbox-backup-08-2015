trigger beforeInsertSalesOrder on PBSI__PBSI_Sales_Order__c (before insert) {
    
    if(Trigger.isbefore && Trigger.isinsert) 
    {
        UpdateProjectInfoForSalesOrder(Trigger.new);
    }
    
    public static void UpdateProjectInfoForSalesOrder(PBSI__PBSI_Sales_Order__c[] salesOrders)
    {
        Map<ID, PBSI__PBSI_Sales_Order__c> map_OppID_SalesOrder = new Map<ID, PBSI__PBSI_Sales_Order__c>();
        
        for (PBSI__PBSI_Sales_Order__c so: salesOrders)
        {
            map_OppID_SalesOrder.put(so.PBSI__Opportunity__c, so);
        }
        
        for(Opportunity opp :[SELECT Has_Welfare_facilities__c, If_yes_welfare_facilities__c, Is_Easy_Access__c, If_no_easy_access__c,
                              Is_site_plan_produced_by_Caloo__c, Are_colours_specified_on_Quote__c, Risk_Assessment_needed__c, Heras_Security_Fencing_needed__c,
                              If_yes_security_fencing__c, Keys_needed__c, Can_the_vehicle_get_to_site__c, If_no_get_to_site__c,
                              Any_other_site_details__c, Skip_Required__c, If_yes_skip_required__c, If_no_skip_required__c,
                              Client_will_meet_installer_on_first_day__c, Are_there_any_variations_to_the_Quote__c, If_yes_quote_variations__c, Method_Statement_needed__c,
                              Site_signage_needed__c, PPE_needed__c, Delivery_times__c, Are_there_parking_restrictions__c, Speed_Limit_mph__c
                              FROM Opportunity WHERE Id IN :map_OppID_SalesOrder.keySet()])
        {
            PBSI__PBSI_Sales_Order__c salesOrder = map_OppID_SalesOrder.get(opp.ID);
            
            salesOrder.Has_Welfare_facilities__c = opp.Has_Welfare_facilities__c;
            salesOrder.If_yes_welfare_facilities__c = opp.If_yes_welfare_facilities__c;
            salesOrder.Is_Easy_Access__c = opp.Is_Easy_Access__c;
            salesOrder.If_no_easy_access__c = opp.If_no_easy_access__c;
            salesOrder.Is_site_plan_produced_by_Caloo__c = opp.Is_site_plan_produced_by_Caloo__c;
            salesOrder.Are_colours_specified_on_Quote__c = opp.Are_colours_specified_on_Quote__c;
            salesOrder.Risk_Assessment_needed__c = opp.Risk_Assessment_needed__c;
            salesOrder.Heras_Security_Fencing_needed__c = opp.Heras_Security_Fencing_needed__c;
            salesOrder.If_yes_security_fencing__c = opp.If_yes_security_fencing__c;
            salesOrder.Keys_needed__c = opp.Keys_needed__c;
            salesOrder.Can_the_vehicle_get_to_site__c = opp.Can_the_vehicle_get_to_site__c;
            salesOrder.If_no_get_to_site__c = opp.If_no_get_to_site__c;
            salesOrder.Any_other_site_details__c = opp.Any_other_site_details__c;
            salesOrder.Is_Skip_Required__c = opp.Skip_Required__c;
            salesOrder.If_yes_skip_required__c = opp.If_yes_skip_required__c;
            salesOrder.If_no_skip_required__c = opp.If_no_skip_required__c;
            salesOrder.Client_will_meet_installer_on_first_day__c = opp.Client_will_meet_installer_on_first_day__c;
            salesOrder.Are_there_any_variations_to_the_Quote__c = opp.Are_there_any_variations_to_the_Quote__c;
            salesOrder.If_yes_quote_variations__c = opp.If_yes_quote_variations__c;
            salesOrder.Method_Statement_needed__c = opp.Method_Statement_needed__c;
            salesOrder.Site_signage_needed__c = opp.Site_signage_needed__c;
            salesOrder.PPE_needed__c = opp.PPE_needed__c;
            salesOrder.Delivery_times__c = opp.Delivery_times__c;
            salesOrder.Are_there_parking_restrictions__c = opp.Are_there_parking_restrictions__c;
            salesOrder.Speed_Limit__c = opp.Speed_Limit_mph__c;
        }
    }
}