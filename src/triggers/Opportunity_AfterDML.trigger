trigger Opportunity_AfterDML on Opportunity (after insert,before insert) {
	if(trigger.isbefore){
		list<PBSI__PBSI_Opportunity_Line__c> lstInsertOpp = new list<PBSI__PBSI_Opportunity_Line__c>();
		for(Opportunity opp : trigger.new){
			if(!string.isBlank(opp.Original_ID__c)){
				opp.PBSI__hasSO__c = '0';
				opp.PBSI__Sales_Order__c =null;
			}
		}
	}
	
	if(trigger.isafter){
		list<PBSI__PBSI_Opportunity_Line__c> lstInsertOpp = new list<PBSI__PBSI_Opportunity_Line__c>();
		for(Opportunity opp : trigger.new){
			if(!string.isBlank(opp.Original_ID__c)){
				list<PBSI__PBSI_Opportunity_Line__c> lstInsertOppLines = [Select Total_Installation__c, Total_Cost__c, Total_Concrete__c, Supplier__c, PBSI__alt_Line_SubTotal__c, PBSI__Unit_of_Measure__c, PBSI__Trade_Agreement__c, PBSI__Trade_AgreementEnabled__c, PBSI__Total_Unit_Quantity__c, PBSI__Total_Number__c, PBSI__Taxable__c, PBSI__Tax_Value__c, PBSI__Status__c, PBSI__Sales_Tax__c, PBSI__Sale_Price__c, PBSI__Quantity__c, PBSI__Price_Type__c, PBSI__Price4__c, PBSI__PrecisioID__c, PBSI__Parent_Line__c, PBSI__Org_Exchange_Rate__c, PBSI__Opportunity__c, PBSI__Opportunity_Line__c, PBSI__OppAccGr__c, PBSI__OPPAccount__c, PBSI__Line_SubTotal__c, PBSI__Line_SubTotal_4__c, PBSI__Line_Status__c, PBSI__Item__c, PBSI__Item_Sales_Price__c, PBSI__Item_Sale_Price4__c, PBSI__Item_Description_Long__c, PBSI__ItemGr__c, PBSI__ItemDescription__c, PBSI__Discount__c, PBSI__Disable_Sales_Trade_Agreement__c, PBSI__Date__c, PBSI__Calculated_Description__c, PBSI__BOM_Type__c, Name, Margin__c, Line_Subtotal_no_discount__c, LastModifiedDate, LastModifiedById, LastActivityDate, Item_Cost__c, IsDeleted, Id, Custom_Cost_Price__c, CurrencyIsoCode, CreatedDate, CreatedById, ConnectionSentId, ConnectionReceivedId From PBSI__PBSI_Opportunity_Line__c where PBSI__Opportunity__c = :opp.Original_ID__c];
				for(PBSI__PBSI_Opportunity_Line__c oppLine : lstInsertOppLines){
					PBSI__PBSI_Opportunity_Line__c ol = oppLine.clone();
					ol.PBSI__Opportunity__c = opp.id;
					ol.Copied_From_c__c = oppLine.Id;
					lstInsertOpp.add(ol);
				}
			}
		}		 
		if(lstInsertOpp.size() > 0){ 
			insert lstInsertOpp;
			system.debug(lstInsertOpp);
		}
	} 
}