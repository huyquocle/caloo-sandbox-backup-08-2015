trigger triggerOpportunityLineUpdate on PBSI__PBSI_Opportunity_Line__c (after insert, after update, after delete) {
    
    Map<id,Opportunity> listOppMap = new Map<id,Opportunity>();
    list<PBSI__PBSI_Opportunity_Line__c> lstOppLines = new list<PBSI__PBSI_Opportunity_Line__c>();
    if( trigger.isupdate )
    {
        for( PBSI__PBSI_Opportunity_Line__c Newitem : trigger.new )
        {
            PBSI__PBSI_Opportunity_Line__c oldItem =  trigger.oldMap.get(Newitem.id);
            if( oldItem.PBSI__Line_SubTotal__c != Newitem.PBSI__Line_SubTotal__c  )
            {
                if(listOppMap.get(Newitem.PBSI__Opportunity__c) == null)
                {
                    listOppMap.put(Newitem.PBSI__Opportunity__c,new Opportunity( id = Newitem.PBSI__Opportunity__c ) );
                }
            }
        }
    }
    else
    {
        if( trigger.isinsert)      
        {
        	set<string> lstOppLine = new set<string>();
        	set<string> olIds = new set<string>();
            for( PBSI__PBSI_Opportunity_Line__c Newitem : trigger.new )
            {
                if(listOppMap.get(Newitem.PBSI__Opportunity__c) == null)
                {
                    listOppMap.put(Newitem.PBSI__Opportunity__c,new Opportunity( id = Newitem.PBSI__Opportunity__c, Has_Opportunity_Line__c = true) );
                }
                
                if(!string.isblank(newItem.Copied_From_c__c)){
                	lstOppLine.add(newItem.Copied_From_c__c);
                	olIds.add(newItem.id);
                }
            }
            
            map<id,PBSI__PBSI_Opportunity_Line__c> map_id_oppLines = new map<id,PBSI__PBSI_Opportunity_Line__c>([select id, PBSI__Item_Description_Long__c,PBSI__ItemDescription__c from PBSI__PBSI_Opportunity_Line__c  where id in: lstOppLine]);
            list<PBSI__PBSI_Opportunity_Line__c> lstOppL = [select id, Copied_From_c__c,PBSI__Item_Description_Long__c,PBSI__ItemDescription__c from PBSI__PBSI_Opportunity_Line__c  where id in: olIds];
            
            for(PBSI__PBSI_Opportunity_Line__c ol : lstOppL){
            	if(map_id_oppLines.containskey(ol.Copied_From_c__c)){
            		PBSI__PBSI_Opportunity_Line__c originalOL = map_id_oppLines.get(ol.Copied_From_c__c);
            		ol.PBSI__ItemDescription__c = originalOL.PBSI__ItemDescription__c;
            		ol.PBSI__Item_Description_Long__c = originalOL.PBSI__Item_Description_Long__c;
            		
            		lstOppLines.add(ol);
            	}
            }
            
            if(lstOppLines.size() > 0)
            	update lstOppLines;
           
             
        }
        else 
        {
            for( PBSI__PBSI_Opportunity_Line__c Deleteitem : trigger.old )
            {
                if(listOppMap.get(Deleteitem.PBSI__Opportunity__c) == null)
                {
                    listOppMap.put(Deleteitem.PBSI__Opportunity__c,new Opportunity( id = Deleteitem.PBSI__Opportunity__c) );
                }
            }
        }
    }
    if( listOppMap.size() > 0)
        update listOppMap.values();
}