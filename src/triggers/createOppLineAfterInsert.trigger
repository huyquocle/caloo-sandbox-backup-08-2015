//Automatically add new item "MON-LEWISHAM-PRELIM" to the line items of any new opp when created.
//Item Number: MON-LEWISHAM-PRELIM, quantity 1.
trigger createOppLineAfterInsert on Opportunity (after insert) {
    List<PBSI__PBSI_Opportunity_Line__c> itemsToCreated = new List<PBSI__PBSI_Opportunity_Line__c>();
    
    List<PBSI__PBSI_Item__c> items = [Select ID from PBSI__PBSI_Item__c where Name='MON-LEWISHAM-PRELIM'];
    if(items.isEmpty()) return;
    
    for(Opportunity opp : Trigger.new){
        PBSI__PBSI_Opportunity_Line__c item = new PBSI__PBSI_Opportunity_Line__c(PBSI__Opportunity__c = opp.ID);
        item.PBSI__Quantity__c      = 1;
        item.PBSI__Item__c          = items.get(0).ID;
        itemsToCreated.add(item);
    }
    
    try{
        insert itemsToCreated;
    }catch(Exception ex){}
}