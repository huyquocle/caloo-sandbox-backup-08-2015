trigger createSitePlanTasks on PBSI__PBSI_Sales_Order__c (after insert) {
	List<Task> tasksToCreate = new List<Task>();
    List<User> users = [select Name from User where Name='Richard McKay' or Name='Juanjo Gallardo'];
    
    for(PBSI__PBSI_Sales_Order__c so : Trigger.new){
        if(so.Is_site_plan_produced_by_Caloo__c=='Yes'){
            if(so.Risk_Assessment_needed__c!='Yes' && so.Method_Statement_needed__c!='Yes' && so.Site_signage_needed__c!='Yes' && so.Heras_Security_Fencing_needed__c!='Yes'){
                if(!users.isEmpty()){
                    for(User u : users){
                        tasksToCreate.add(
                            new Task(
                                Subject		= 'You need to complete the site plan requirements on SO ' + so.Name, 
                                OwnerId		= u.ID,
                                ActivityDate = so.PBSI__Due_Date__c,
                                WhatId		= so.ID
                            )
                        );
                    }
                }    
            }else{
                tasksToCreate.add(
                    new Task(
                        Subject		= getTaskSubject(so), 
                        OwnerId		= so.OwnerId,
                        ActivityDate = so.PBSI__Due_Date__c,
                        WhatId		= so.ID
                    )
                );
            }
        }
    }
    //Insert task, send notification
    Database.DMLOptions dmlo = new Database.DMLOptions();
	dmlo.EmailHeader.triggerUserEmail = true;
    Database.insert(tasksToCreate, dmlo);
    
    public String getTaskSubject(PBSI__PBSI_Sales_Order__c so){
        String subject = '';
        if(so.Risk_Assessment_needed__c=='Yes') subject = 'Risk Assessment';
        else if(so.Method_Statement_needed__c=='Yes') subject = 'Method Statement';
        else if(so.Site_signage_needed__c=='Yes') subject = 'Site signage';
        else if(so.Heras_Security_Fencing_needed__c=='Yes') subject = 'Heras Security Fencing';
        
        subject += ' is required, please action.';
        return subject;
    }
}
/*
 * Essentially site plan is a filed which is ticked yes or no ( it essentially means they build playgrounds and it means that they have done allan for the site or not). Then there is a section of site requirements which are 4 questions each with a yes or a no….so
==> Site plan is a field of which object? I'm not able to find it on object like Account, Sales Order...

Sales Order, it's one of the fields from the job in case 10213

If the site plan is ticked yes and any of the site requirements are ticked yes..then the system should send a task to the owner of the SO dated the rule trigger date. The task name will be the {FIELD NAME} that Is required from the requirements,
==>Will the task be sent 1 time or multiple times? For example, if site plan is yes, one of site requirements is ticked then the record is saved, then another site requirements is ticked...

OK, send 1 task, saying "You need to complete the site plan requirements on SO {SO Name} if any of the fields are not filled in. only once, and only if the site plan is required field is set to yes and any of the other 4 fields are not filled in.

==>I assume 'send a task to the owner of the SO' means 'create a task, assign to the owner of the SO', correct? If yes, is it necessary to notify the owner?

Yes

There is an exception if site plan yes with no requirements then it needs to go to to go to Richard AND Juanho as a task.
==> Create 2 tasks, assign 1 task to Richard and the other one to Juanho? ==> What is the task's name?

Yes, same task as above "You need to complete the site plan requirements on SO {SO Name}
*/