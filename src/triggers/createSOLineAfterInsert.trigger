//If a new sales order is created and it doesn't have an opportunity or quote associated with it, add the item to a new SO line 
//Item Number: MON-LEWISHAM-PRELIM, quantity 1.
trigger createSOLineAfterInsert on PBSI__PBSI_Sales_Order__c (after insert) {
    List<PBSI__PBSI_Sales_Order_Line__c> linesToCreated = new List<PBSI__PBSI_Sales_Order_Line__c>();
    
    List<PBSI__PBSI_Item__c> items = [Select ID from PBSI__PBSI_Item__c where Name='MON-LEWISHAM-PRELIM'];
    if(items.isEmpty()) return;
    
    for(PBSI__PBSI_Sales_Order__c so : Trigger.new){
        if(so.PBSI__Opportunity__c==null && so.PBSI__From_Quote__c==null){
            PBSI__PBSI_Sales_Order_Line__c line = new PBSI__PBSI_Sales_Order_Line__c();
            line.PBSI__Sales_Order__c   = so.ID;
            line.PBSI__Quantity__c      = 1;
            line.PBSI__Quantity_Needed__c   = 1;
            line.PBSI__Item__c          = items.get(0).ID;
            linesToCreated.add(line);
        }
    }
    
    try{
        insert linesToCreated;
    }catch(Exception ex){}
}