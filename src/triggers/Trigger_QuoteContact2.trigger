trigger Trigger_QuoteContact2 on PBSI__Quote__c (before insert)
{
    QuoteContact.updateQuoteContact(Trigger.New);
    /*
List<Id> listQuotes = new List<Id>();
for(PBSI__Quote__c updatedQuote : Trigger.new)
{
listQuotes.add(updatedQuote.Id);
}
QuoteContact.updateQuoteContact(listQuotes);
*/
    PopulateHeadingGroups(Trigger.New);
    
    private void PopulateHeadingGroups(List<PBSI__Quote__c> quotes) {
        Set<Id> oppIds = new Set<ID>();
        
        for (PBSI__Quote__c q:quotes) {
            oppIds.add(q.PBSI__Opportunity__c);
        }
        
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>([SELECT ID, Heading_Groups__c, ProjectDiscount__c,Site_name_product_range__c  FROM Opportunity WHERE ID IN :oppIds]);
        
        if (mapOpp.size() > 0) {
            for (PBSI__Quote__c q:quotes) {
                if( q.PBSI__Opportunity__c != null)
                {
                    Opportunity opp = mapOpp.get(q.PBSI__Opportunity__c);
                	q.Heading_Group__c = opp.Heading_Groups__c;
                    q.Caloo_Project_Discount1__c = opp.ProjectDiscount__c;
                    q.SiteName1__c = opp.Site_name_product_range__c;
                }
                
            }
        }
    }
}