trigger notifyTotalQuantToShipChange on PBSI__PBSI_Sales_Order_Line__c (after insert, after update, after delete) {
	Set<ID> soIDs = new Set<ID>();//List of Sales Order which Total Quant To Ship is changed
    if(Trigger.isInsert){
        for(PBSI__PBSI_Sales_Order_Line__c soline : Trigger.new){
            if(soline.PBSI__Quantity_Left_To_Ship__c > 0){
                soIDs.add(soline.PBSI__Sales_Order__c);
            }
        }
    }
    
    if(Trigger.isUpdate){
        PBSI__PBSI_Sales_Order_Line__c oldLine;
        for(PBSI__PBSI_Sales_Order_Line__c newLine : Trigger.new){
            oldLine	= Trigger.oldMap.get(newLine.Id);
            if(oldLine.PBSI__Quantity_Left_To_Ship__c != newLine.PBSI__Quantity_Left_To_Ship__c){
                soIDs.add(newLine.PBSI__Sales_Order__c);
            }
        }
    }
    
    if(Trigger.isDelete){
        for(PBSI__PBSI_Sales_Order_Line__c soline : Trigger.old){
            if(soline.PBSI__Quantity_Left_To_Ship__c > 0){
                soIDs.add(soline.PBSI__Sales_Order__c);
            }
        }
    }
    
    if(soIDs.isEmpty()) return;
    
    try{
        //Update SOs, increalse 'Is Total Quant To Ship Change' by 1 to trigger a workflow rule to send an email alert to Kate Rance
        List<PBSI__PBSI_Sales_Order__c> soList = [Select Total_Quant_To_Ship_Change_Tracking__c from PBSI__PBSI_Sales_Order__c where ID IN :soIDs ];
        for(PBSI__PBSI_Sales_Order__c so : soList){
            if(so.Total_Quant_To_Ship_Change_Tracking__c==null) so.Total_Quant_To_Ship_Change_Tracking__c = 1;
            else so.Total_Quant_To_Ship_Change_Tracking__c += 1;
        }
        update soList;
    }catch(Exception ex){}
}