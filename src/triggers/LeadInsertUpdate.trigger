/* 
 * LeadInsertUpdate is the trigger on Lead object insert and edit 
 * Functionality :
 *  
 * When Leads with field "Zip/Postal Code" are created or edited trigger gets fired
 * Revision History: 
 *
 * Version    	 Author        	   Date         Description 
 *  1.0        Ajay Ghuge       14/02/2014     Initial Draft 
 */
 
trigger LeadInsertUpdate on Lead (before insert, before update) 
{
	LeadInsertUpdateHandler objHandler = new LeadInsertUpdateHandler();
	if((Trigger.isInsert && Trigger.isBefore) || (Trigger.isBefore && Trigger.isUpdate))
	{
		// Trigger will Update the fields on Lead on Insert or Update
		objHandler.getSalesAreafromLead(Trigger.new);
	}
}