trigger populateQuoteData on PBSI__PBSI_Sales_Order__c (before Insert, before Update) {
    if(Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate) ){
        List<PBSI__PBSI_Sales_Order__c> soToProcess = new List<PBSI__PBSI_Sales_Order__c>();
        List<ID> quoteIDs = new List<ID>();
        //Identify Sales Order which PBSI__From_Quote__c is populated at the first time.
        if(Trigger.isInsert){
            for(PBSI__PBSI_Sales_Order__c so : Trigger.new){
                if(so.PBSI__From_Quote__c<>null){
                    soToProcess.add(so);
                    quoteIDs.add(so.PBSI__From_Quote__c);
                }
            }
        }else{//Update
            PBSI__PBSI_Sales_Order__c oldRec;
            for(PBSI__PBSI_Sales_Order__c  newRec : Trigger.new){
                oldRec    = Trigger.oldMap.get(newRec.ID);
                if(oldRec.PBSI__From_Quote__c==null && newRec.PBSI__From_Quote__c<>null){
                    soToProcess.add(newRec);
                    quoteIDs.add(newRec.PBSI__From_Quote__c);
                }
            }
        }
        //Copy data from Quote to Sales Order
        if(!quoteIDs.isEmpty()){
            Map<ID, PBSI__Quote__c> quoteMap = new Map<ID,PBSI__Quote__c>([Select Name, Concrete_Cost__c, Concrete_Sell__c, Installation_Cost__c, Installation_Sell__c from PBSI__Quote__c where ID IN :quoteIDs]);
            
            PBSI__Quote__c tmpQuote;
            for(PBSI__PBSI_Sales_Order__c so : soToProcess){
                if(quoteMap.containsKey(so.PBSI__From_Quote__c)){
                    tmpQuote    = quoteMap.get(so.PBSI__From_Quote__c);
                    so.Concrete_Cost__c        = tmpQuote.Concrete_Cost__c;
                    so.Concrete_Sell__c        = tmpQuote.Concrete_Sell__c;
                    so.Installation_Cost__c    = tmpQuote.Installation_Cost__c;
                    so.Installation_Sell__c    = tmpQuote.Installation_Sell__c;
                    
                }
            }
        }
    }
}