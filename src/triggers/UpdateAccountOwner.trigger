trigger UpdateAccountOwner on Account (before Insert) {
    if(trigger.isinsert)
    {
        //Get Postcode to List
        Map<string,Postcodes__c> postsaleMap = new Map<string,Postcodes__c>();       
        for(Postcodes__c postcodeItem :[select Name, sales_area__c,ownerid from Postcodes__c]) 
        {
            if(postsaleMap.get(postcodeItem.Name) == null)
            {
                postsaleMap.put(postcodeItem.Name,postcodeItem);
            }
        }
        
        for(Account acc:trigger.new)
        { 
            if(acc.BillingPostalCode != null 
               && acc.Category__c =='Customer' 
               && acc.BillingPostalCode.length() > 0)
            { 
                string tmp = '';
                if(acc.BillingPostalCode.length() >2)
                {
                    tmp = acc.BillingPostalCode.substring(0,2);
                }
                else
                {
                    tmp = acc.BillingPostalCode.trim();
                }
                if(!tmp.isnumeric())
                {
                    if(tmp.length() == 2)
                    {
                        if(tmp.substring(0,1).isnumeric())
                        {
                            tmp = tmp.substring(1,1);
                        }
                        else if(tmp.substring(1,1).isnumeric())
                        {
                            tmp = tmp.substring(0,1);
                        }
                    }
                    Postcodes__c pc =  postsaleMap.get(tmp);
                    if(pc!=null)
                    {
                        system.debug('pc1='+pc);
                        acc.Sales_Area__c = pc.Sales_Area__c;
                        acc.OwnerId = pc.OwnerId;
                    }
                }
            }
        }
    }
}