trigger Trigger_SalesOrderContact on PBSI__PBSI_Sales_Order__c (before insert) {
	SalesOrderContact.updateSalesOrderContact(Trigger.New);
}