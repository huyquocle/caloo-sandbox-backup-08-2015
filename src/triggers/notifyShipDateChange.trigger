trigger notifyShipDateChange on PBSI__PBSI_Sales_Order__c (after update) {
    Set<ID> soIDs = new Set<ID>();
    PBSI__PBSI_Sales_Order__c oldRec;
    for(PBSI__PBSI_Sales_Order__c newRec : Trigger.new){
        oldRec    = Trigger.oldMap.get(newRec.ID);
        if(oldRec.PBSI__Due_Date__c != newRec.PBSI__Due_Date__c){
            soIDs.add(newRec.ID);
        }    
    }
    
    List<PBSI__PBSI_Purchase_Order__c> poToProcess = [Select ID, PBSI__Contact__c from PBSI__PBSI_Purchase_Order__c where PBSI__Status__c='Open' and PBSI__Drop_Ship_Sales_Order__c IN :soIDs and PBSI__Contact__r.Email<>null];
    if(!poToProcess.isEmpty()){
        Id et = TriggerUtils.getEmailTemplateId('Ship_Date_change_Notify_Supplier');
        if(et==null) return;
        List<Messaging.SingleEmailMessage> emailList = new List<Messaging.SingleEmailMessage>();
        for(PBSI__PBSI_Purchase_Order__c po : poToProcess){
             emailList.add(TriggerUtils.generateEmail(et, po.ID, po.PBSI__Contact__c, false));
        }
        try{
            Messaging.sendEmail(emailList);
        }catch(Exception ex){}
    }
}