trigger UpdateOppAfterUpdate on PBSI__PBSI_Opportunity_Line__c (After update, After insert, after delete) {   
    
    
    
    set<id> oppIdList = new set<id>();
    
    
    
    
    if( trigger.isupdate || trigger.isinsert)
    {
	    for(PBSI__PBSI_Opportunity_Line__c newItem:trigger.new)
	    {
	        if(!oppIdList.contains(newItem.PBSI__Opportunity__c))
	        {
	            oppIdList.add(newItem.PBSI__Opportunity__c);
	        }
	    }
    }
    if( trigger.isdelete)
    {
	    for(PBSI__PBSI_Opportunity_Line__c newItem:trigger.old)
	    {
	        if(!oppIdList.contains(newItem.PBSI__Opportunity__c))
	        {
	            oppIdList.add(newItem.PBSI__Opportunity__c);
	        }
	    }
    }
    
    Map<id,Opportunity> oppList = new Map<id,Opportunity>([SELECT ID,Oppline_Value__c,PBSI__Sales_Order__c 
                                                           from Opportunity 
                                                           where  id in:oppIdList
                                                           and PBSI__Sales_Order__c = null]);
    for( Opportunity item : oppList.values())
    {
    	item.Oppline_Value__c = 'Green';
    }                                                       
    
    for(PBSI__PBSI_Opportunity_Line__c item: [select id,Total_Cost__c,Margin__c,
                                              PBSI__Opportunity__c,
                                              PBSI__Item__r.PBSI__Item_Group__r.Margin__c,
                                              PBSI__Line_SubTotal__c
                                              from PBSI__PBSI_Opportunity_Line__c 
                                              where PBSI__Opportunity__c in:oppIdList])
    {
        if(oppList.get(item.PBSI__Opportunity__c) != null)
        {
            //oppList.get(item.PBSI__Opportunity__c).Oppline_Value__c = 'Green';
            /*if(item.PBSI__Line_SubTotal__c  !=null && item.PBSI__Line_SubTotal__c !=0 && item.Total_Cost__c != null)
            {
                decimal mar = 0;
                if( item.PBSI__Item__r.PBSI__Item_Group__r.Margin__c != null)
                {
                    mar =  item.PBSI__Item__r.PBSI__Item_Group__r.Margin__c;
                }
                if(((item.PBSI__Line_SubTotal__c  - item.Total_Cost__c) / item.PBSI__Line_SubTotal__c )*100 
                   < mar)
                {  
                    if(!redList.contains(item.PBSI__Opportunity__c))
                    {
                      redList.add(item.PBSI__Opportunity__c);    
                    }                                                  
                }            
            } */
            if(item.Margin__c != null && item.Margin__c.contains('flag_red.gif'))
            {
                oppList.get(item.PBSI__Opportunity__c).Oppline_Value__c = 'Red';
            }            
        }        
    }
   update oppList.values();
}