trigger UpdateSurveyInfor on Site_Survey__c (before insert) {
    
    if( Trigger.isBefore && Trigger.isInsert )
    {
        set<id> oppId = new set<id>();
        for(Site_Survey__c item: trigger.new)
        {
            if(!oppid.contains(item.Opportunity__c))
            {
                oppId.add(item.Opportunity__c);                
            }
        }
        List<Opportunity> oppList = [SELECT ID,Account.Name,PBSI__DeliveryAddress__c, (SELECT contactId,contact.Phone, contact.name
                                                                                       FROM opportunitycontactroles
                                                                                       WHERE IsPrimary = TRUE) 
                                     FROM Opportunity where id=:oppId];
        
        for(Opportunity opItem:oppList)
        {
            //opportunitycontactroles ctL = opItem.opportunitycontactroles__r;
            for(Site_Survey__c sItem:trigger.new)
            {
                if(sitem.Opportunity__c == opItem.id)
                {
                    sitem.Client_Company_Name__c = opItem.Account.Name;
                    if(opItem.opportunitycontactroles != null && opItem.opportunitycontactroles.size() > 0)
                    {
                        sitem.Client_Contact_Name__c = opItem.opportunitycontactroles[0].Contact.Name;
                        sitem.Contact_Telephone_Number__c = opItem.opportunitycontactroles[0].Contact.Phone;
                    }
                    sitem.Site_Address__c = opItem.PBSI__DeliveryAddress__c;
                }
            }
        }
    }
}