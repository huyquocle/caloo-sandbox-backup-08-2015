trigger afterInsUpdItem on PBSI__PBSI_Item__c (after update, after delete) {
    
    if(Trigger.isafter && (Trigger.isupdate || Trigger.isdelete))
    {
        /*force update PBSI__BOM_Line__c*/
        update [select id from PBSI__BOM_Line__c where PBSI__BOM_Line__c.PBSI__Item__c IN :Trigger.OldMap.Keyset()];
    }
}