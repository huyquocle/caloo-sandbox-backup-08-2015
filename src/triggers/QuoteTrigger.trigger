trigger QuoteTrigger on PBSI__Quote__c (before insert, before update) {
	for( PBSI__Quote__c item : trigger.New )
    {
        if(  !string.ISEmpty(item.Heading_Group__c))
            item.Is_Empty_Heading__c = true;
    }
}