Hello {!Contact.FirstName},

Further to your enquiry, please find attached our play and fitness equipment brochures for your consideration with price guides. 

Please follow the link and use the large arrow buttons on either side of the screen to navigate between documents.  Select the download button at the bottom of the screen if required.

https://emea.salesforce.com/sfc/p/20000000BUqUksiA6lgn5fHYl212EF_NYxgX79w= 

The prices are CIF (delivered) prices to your most local sea port and are based on product being shipped on a shared container basis (LCL) and subject to a minimum order value of £8000.00. For clarity, you will not have any shipping costs from our factory to your local sea port. I trust this makes our rates even more competitive. On arrival, you will be responsible for any import duty, any local taxes, port charges and delivery to your site / warehouse. 

Should you require 3 or more play units or a minimum of 32 items of fitness equipment on a single order and shipment, I can offer you a discount and we will ship the products on a full container basis (FCL) so the product will not share the container with any other Company or product.  

To confirm your discount, please advise your product selection and the name and Country location of your closest sea port and I will be happy to provide a quotation. 

Playground Equipment 

All our playground units are manufactured in accordance with the current European Standard EN1176 and offer the highest material specifications and excellent value for money. 

Each multi-play unit features:-

•	Galvanised and powder coated posts offering the highest anti-corrosion protection 
•	Large 1000mm x 1000mm square heavy duty steel platforms enabling high user capacity – ideal for a leisure facility, school or park 
•	Rubber coated platforms which are warm to touch in Winter and cool in Summer. Ideal for extended use in all climates 
•	Bright, strong and durable UV stable rotomoulded slides, barriers, seats and games 
•	Vandal resistant 
•	Disability Discrimination Act compliant offering play opportunity for children of all abilities 

Caloo Outdoor Fitness Equipment

All Caloo fitness equipment is manufactured predominately from steel to withstand heavy use during operation and also to perform, without issue, in the ‘free access’ locations often selected in the wide range of installed environments. With resistance offered from user weight, these carbon neutral, human powered units are environmentally friendly and great fun.

Our products are suitable for use in any outdoor environment where fitness is to be encouraged. Examples of applications are schools, parks, beaches, community spaces, leisure spaces, social clubs, factories and housing areas where combinations are used to create either a full body workout or fun facility.   

Our range is designed to provide exercise opportunity for persons aged 7 – 107 years.  

Our range features:-

•	Units certified by TUV to 55012:2010 Training Equipment Placed in Public Spaces. Follow this link for further information about this standard - http://www.tuv-sud.cn/outdoor-fitness-equipment.html   
•	The highest raw material specification – increased for European and Middle Eastern Markets.  
•	Posts are manufactured from 3.2mm – 5.0mm thick CHS mild steel, primed and electrostatically powder coated to clients choice of colours. 
•	Metal foundation covers and post caps supplied as standard. 
•	Selected items feature rubber movement limiters to reduce opportunity of injury through over stretching or damage through misuse. 
•	Bearings are sealed steel units which require no regular maintenance. 
•	Footrests (where used) are cast steel with anti slip grip pattern 
•	Clamps, (where used), are manufactured from Aluminium alloy which is powder coated as above. Collars and clamps feature recessed fixing positions. 
•	Usage instructions are attached to each unit encouraging safe and efficient use of each item. 
•	Comprehensive product guarantee that includes paint process. 
•	Low maintenance requirements with no regular maintenance required. 
•	National and International installation service. 
•	£5m product and public liability insurance. 
•	Track record in municipal and educational markets. 

Should you proceed to order, we currently have a 4 week manufacturing lead time after receipt of cleared funds. Shipping lead times are not included as the time from shipping until arrival depends on your port location. 

We are able to provide installation instructions to assist with the construction of units.

I look forward to the opportunity of supplying you our products for resale on in your market.  

Kind regards,