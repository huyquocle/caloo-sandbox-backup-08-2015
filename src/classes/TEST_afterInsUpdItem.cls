@isTest
private class TEST_afterInsUpdItem {
    private static testMethod void testTrigger(){
		PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
      	insert itemGroup;
    
     	PBSI__PBSI_Location__c loc=new PBSI__PBSI_Location__c(Name='asfafasf');  
     	insert loc;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c(Name='MON-LEWISHAM-PRELIM');
        item.CurrencyIsoCode	= 'GBP';
        item.PBSI__Default_Location__c	= loc.ID;
        item.PBSI__Item_Group__c		= itemGroup.ID;
        item.PBSI__description__c		= 'description';
        item.Concrete_Volume__c = 1;
        insert item;
        
        item.Concrete_Volume__c = 2;
        update item;
    }
}