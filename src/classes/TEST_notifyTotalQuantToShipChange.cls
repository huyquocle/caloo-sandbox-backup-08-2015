@isTest
private class TEST_notifyTotalQuantToShipChange{
	private static testMethod void testTrigger()
	{
        Test.startTest();
        
        PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c(PBSI__Due_Date__c = Date.today());
        insert so;
        
        PBSI__PBSI_Location__c location = new PBSI__PBSI_Location__c(
            Name			='Test',
            CurrencyIsoCode	= 'GBP'
        );
        insert location;
        
        PBSI__PBSI_Item_Group__c itemgroup = new PBSI__PBSI_Item_Group__c(
            Name			='Test',
            CurrencyIsoCode	= 'GBP',
            PBSI__Item_Group_Code__c = 'abc'
        );
        insert itemgroup;
            
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c(
        	Name 						= 'Test',
            PBSI__description__c		= 'Description',
            PBSI__defaultunitofmeasure__c = 'm',
            PBSI__Default_Location__c 	= location.ID,
            PBSI__Item_Group__c			= itemgroup.ID,
            CurrencyIsoCode 			= 'GBP'
        );
        insert item;
        
        PBSI__PBSI_Sales_Order_Line__c soLine = new PBSI__PBSI_Sales_Order_Line__c(
        	PBSI__Sales_Order__c	= so.ID,
            PBSI__Quantity_Needed__c	= 6,
            PBSI__Quantity_Picked__c	= 4,
            CurrencyIsoCode	= 'GBP',
            PBSI__Item__c	= item.ID
        );
        insert soLine;
        
        soLine.PBSI__Quantity_Needed__c	= 10;
        update soLine;
        
        //delete soLine;
        
        Test.stopTest();
    }
}