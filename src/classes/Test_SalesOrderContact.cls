@isTest
private class Test_SalesOrderContact{
    static testMethod void test(){
        
        Contact c = new Contact(lastname='Test contact');
        insert c;
        
        Account a = new Account(name='Test acc', Location__c='Bristol', Category__c='Media');
        insert a;
        
        Opportunity o = new Opportunity(name='Test opp', Account=a, Contact__c=c.Id, stagename='HotList', closedate=date.parse('12/12/2014'), LeadSource='Other');
        insert o;
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.Id, OpportunityId = o.Id, IsPrimary = true);
        insert ocr;
        
        PBSI__PBSI_Sales_Order__c q = new PBSI__PBSI_Sales_Order__c(PBSI__Opportunity__c = o.id); 
        insert q;
        q = [SELECT PBSI__Contact__c, PBSI__Opportunity__c, Name FROM PBSI__PBSI_Sales_Order__c WHERE Id = :q.Id];
        
        System.assertEquals(q.PBSI__Contact__c, c.id);
    }
}