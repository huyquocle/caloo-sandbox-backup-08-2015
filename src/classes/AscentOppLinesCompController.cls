public without sharing class AscentOppLinesCompController {
    public string OppID {get;set;}
    
    private list<PBSI__PBSI_Opportunity_Line__c> pOppLines;
    private Opportunity pOpp;
    private Contact pContact;
    
    public list<PBSI__PBSI_Opportunity_Line__c> OppLines{
        get {
            if (pOppLines == NULL) {
                pOppLines = [SELECT Id, PBSI__Item__r.Name, PBSI__Sale_Price__c, Total_Cost__c, PBSI__Quantity__c FROM PBSI__PBSI_Opportunity_Line__c WHERE PBSI__Opportunity__c = :OppID];            
            }
            return pOppLines;
        }
        set;
    }
    
    public Opportunity Opp {
        get {
            if (pOpp == NULL) {
                if (OppID != NULL)
                	pOpp = [SELECT ID, Account.Name, Total_Cost__c, Caloo_Final_Order_Total__c, Margin_Amount__c, Margin__c FROM Opportunity WHERE ID =:OppID];
                else
                    return NULL;
            }
            return pOpp;
        }
        set;
    }
    
    public Contact PrimaryContact {
        get {
            if (pContact == NULL) {
                OpportunityContactRole pPrimaryContact = [select id,role,contactid from OpportunityContactRole where OpportunityID = :OppID AND isprimary=true];
                if (pPrimaryContact != NULL) {
                    pContact = [SELECT Id, Name FROM Contact WHERE ID =:pPrimaryContact.ContactId];
                }
            }
            return pContact;
        }
        set;
        
    }
    
    
    public AscentOppLinesCompController(){        
    }
}