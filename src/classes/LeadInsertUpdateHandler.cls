/* 
 * LeadInsertUpdateHandler is the trigger on Lead object insert and edit
 *
 * Functionality :
 *  
 * On Insert when Lead is created with 'Zip/Postal Code', its related 'Postcode Area' is populated 
 * And related to that its "Sales Area" field should get populated
 * 
 * On Update Lead's Zip/Postal Code is updated, its related 'Postcode Area' is updated
 * And related to that its "Sales Area" field should get updated
 *
 * Revision History: 
 *
 * Version       Author             Date         Description 
 *  1.0        Ajay Ghuge      14/02/2014      Initial Draft 
 */

public with sharing class LeadInsertUpdateHandler 
{
  
  public void getSalesAreafromLead(Lead[] lstLead)
  {  // Set of Strings having PostCode Area from lead
    Set<String> leadPostalAreaSet = new Set<String>();
    for(Lead objLead : lstLead)
    {
      if(objLead.PostalCode != Null)
      {
        leadPostalAreaSet.add(objLead.Postcode_Area__c.trim());
      }// end of if
    }// end of for
    
    //Getting the Sales Area from the PostCodes
    Map<String, String> mapPostCode = new Map<String, String>();
    for(PostCodes__c postCode : [SELECT Name, Sales_Area__c FROM PostCodes__c WHERE Name IN: leadPostalAreaSet])
    {
      mapPostCode.put(postCode.Name, postCode.Sales_Area__c);
    }
    
    //Assigning the Sales_Area__c field from the PostCodes to the Sales_Area__c of the Lead field
    for(Lead objLead : lstLead)
    {
      if(mapPostCode!= NULL && mapPostCode.containskey(objLead.Postcode_Area__c) 
        && mapPostCode.get(objLead.Postcode_Area__c) != NULL)
        {
          objLead.Sales_Area__c = mapPostCode.get(objLead.Postcode_Area__c.trim());
        }
    }// end of for
  }// end of method getSalesAreafromLead
}