public class OpportunityContactUtil{
    public static Map<ID, ID> getSingleContactForOpps(Set<ID> setOppId){
    	Map<ID, OcrWrapper> mapOppOcr = new Map<ID, OcrWrapper>();
        Map<ID, ID> mapOppOcrId = new Map<ID, ID>();
        
        for(OpportunityContactRole varOcr :[SELECT ContactId, OpportunityId,IsPrimary FROM OpportunityContactRole WHERE OpportunityID in :setOppId]){
            if(!mapOppOcr.containsKey(varOcr.OpportunityId)){
                mapOppOcr.put(varOcr.OpportunityID, new OcrWrapper(varOcr));
            }
            else if(mapOppOcr.get(varOcr.OpportunityId).OCR.IsPrimary == false){
                mapOppOcr.get(varOcr.OpportunityId).IsSingle = false || (varOcr.IsPrimary == true);
                mapOppOcr.get(varOcr.OpportunityId).Ocr = varOcr;
            }
        }
        for(ID varOppId :mapOppOcr.keySet()){
            if(mapOppOcr.get(varOppId).IsSingle){
                mapOppOcrId.put(varOppId, mapOppOcr.get(varOppId).Ocr.ContactId);
            }
        }
        
        return mapOppOcrId;
    }
    
    public class OcrWrapper{
        public OpportunityContactRole Ocr{get;set;}
        public Boolean IsSingle{get;set;}
        
        public OcrWrapper(OpportunityContactRole ocr){
            this.Ocr = ocr;
            this.IsSingle = true;
        }
    }
}