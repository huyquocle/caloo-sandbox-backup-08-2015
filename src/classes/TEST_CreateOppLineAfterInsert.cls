@isTest(SeeAllData=true)
public class TEST_CreateOppLineAfterInsert{
	private static testMethod void testTrigger(){
        /*
        User_Names__c usernameTest = new User_Names__c();
    	usernameTest.Name='User';
    	usernameTest.UserName__c ='Rob Bradshaw';
    	insert usernameTest;
    	
    	Postcode__c postcodecustomsetting = new Postcode__c();
    	postcodecustomsetting.Name ='Owner12';
    	postcodecustomsetting.SalesArea__c = '1';
    	postcodecustomsetting.UserName__c = 'Luke Overall';
    	insert  postcodecustomsetting;
    	
    	PostCodes__c postcodeTest = new PostCodes__c();
    	postcodeTest.Name = 'test123';
    	postcodeTest.CurrencyIsoCode='EUR';
    	postcodeTest.Sales_Area__c ='1';
        */
        Account acc = new Account();
    	acc.Name = 'test';
    	acc.CurrencyIsoCode ='EUR';
    	acc.Location__c = 'CornWall';
    	acc.Category__c ='Media';
    	insert acc;
    	
    	Contact con = new Contact();
    	con.LastName = 'abc';
    	con.AccountId = acc.Id;
    	con.CurrencyIsoCode ='EUR';
    	insert con;
    	
    	Opportunity opp = new Opportunity();
    	opp.Name='Test opportunity';
    	opp.AccountId = acc.Id;
    	opp.Contact__c = con.Id;
        opp.Type = 'New Business';
        opp.PBSI__DeliveryPostalCode__c = 'CO9';
    	opp.CloseDate = Date.valueOf('2013-12-12');
    	opp.StageName='Prospecting';
      	opp.LeadSource = 'Other';
      	
      	Test.startTest();
      	insert opp;
        
        Test.stopTest();
    }
}