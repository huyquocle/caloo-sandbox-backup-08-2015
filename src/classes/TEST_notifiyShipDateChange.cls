@isTest
public class TEST_notifiyShipDateChange{
    private static TestMethod void test(){
    	Map<String, SObject> entities = TEST_notifiyShipDateChange.initData();
    }
    
    public static Map<String, SObject> initData(){
        Map<String, SObject> entities = new Map<String, SObject>();
        
        Contact contact1 = new Contact(Email = 'testemail@email.com', LastName = 'LastName');
        insert contact1;
        entities.put('contact1',contact1);
        
        PBSI__PBSI_Sales_Order__c salesOrder1 = new PBSI__PBSI_Sales_Order__c(PBSI__Due_Date__c = Date.today());
        insert salesOrder1;
        entities.put('salesOrder1', salesOrder1);
        
        PBSI__PBSI_Purchase_Order__c purchaseOrder1 = new PBSI__PBSI_Purchase_Order__c(PBSI__Contact__c = contact1.Id, PBSI__Drop_Ship_Sales_Order__c = salesOrder1.Id, PBSI__Status__c='Open');        
        insert purchaseOrder1;
        entities.put('purchaseOrder1', purchaseOrder1);
        
        salesOrder1.PBSI__Due_Date__c.addDays(1);
        update salesOrder1;
        entities.put('salesOrder1', salesOrder1);
        
    	return entities;
    }
}