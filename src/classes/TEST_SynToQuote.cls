@isTest
private class TEST_SynToQuote {
    
    static testMethod void myUnitTest() {
        List<Quote_Adjustment_Setting__c> settings = new List<Quote_Adjustment_Setting__c>();
        Quote_Adjustment_Setting__c setting1 = new Quote_Adjustment_Setting__c(
            Name = '1',
            Object_Name__c = 'Opportunity',
            Source_Field__c = 'AccountId',
            Target_Field__c = 'PBSI__Account__c'
        );
        settings.add(setting1);
        Quote_Adjustment_Setting__c setting2 = new Quote_Adjustment_Setting__c(
            Name = '2',
            Object_Name__c = 'Opportunity',
            Source_Field__c = 'ProjectDiscount__c',
            Target_Field__c = 'Caloo_Project_Discount1__c'
        );
        settings.add(setting2);
        Quote_Adjustment_Setting__c setting3 = new Quote_Adjustment_Setting__c(
            Name = '3',
            Object_Name__c = 'OpportunityLine',
            Source_Field__c = 'PBSI__Item__c',
            Target_Field__c = 'PBSI__Item__c'
        );
        settings.add(setting3);
        insert settings;
        
        Account acc = new Account();
        acc.Name = 'test';
        acc.CurrencyIsoCode ='EUR';
        acc.Location__c = 'CornWall';
        acc.Category__c ='Media';
        insert acc;
        
        Contact con = new Contact();
        con.LastName = 'abc';
        con.AccountId = acc.Id;
        con.CurrencyIsoCode ='EUR';
        insert con;
        
        Opportunity opp = new Opportunity();
        opp.Name='Test opportunity';
        opp.AccountId = acc.Id;
        opp.Contact__c = con.Id;
        opp.Type = 'New Business';
        opp.PBSI__DeliveryPostalCode__c = 'CO9';
        opp.CloseDate = Date.valueOf('2013-12-12');
        opp.StageName='Prospecting';
        opp.LeadSource = 'Other';
        opp.Site_Name__c = 'testsite';
        opp.ProjectDiscount__c = 500;
        insert opp;
        
        PBSI__PBSI_Location__c location = new PBSI__PBSI_Location__c();
        location.name = 'test';
        insert location;
        
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c();
        itemGroup.Name = 'test';
        itemGroup.Is_Edit_Group__c = true;
        insert ItemGroup;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Item_Group__c = itemGroup.id;
        item.Name = 'test';
        item.PBSI__Cost__c = 100;
        item.PBSI__description__c = 'test description';
        item.PBSI__Default_Location__c = location.id;
        insert item;
        
        PBSI__PBSI_Opportunity_Line__c oppLine = new PBSI__PBSI_Opportunity_Line__c(
        	PBSI__Opportunity__c = opp.Id,
            PBSI__Item__c = item.Id,
            PBSI__Sale_Price__c = 100,
            PBSI__Quantity__c = 1
        );
        insert oppLine;
        
        OpportunityContactRole opr = new OpportunityContactRole();
        opr.ContactId = con.id;
        opr.OpportunityId = opp.id;
        opr.IsPrimary = true;
        insert opr;
        
        PBSI__Quote__c quote = new PBSI__Quote__c(
            Name='Test Quote', 
            CurrencyIsoCode='GBP', Heading_Group__c = 'temp 1,temp 2, temp 3',
            PBSI__Opportunity__c = opp.id);
        insert quote;
        PBSI__Quote_Line__c line = new PBSI__Quote_Line__c(PBSI__Quote__c = quote.Id, Heading_Group__c = 'temp 1', Display_Order__c = 1);
        insert line;        
        
        ApexPages.CurrentPage().getparameters().put('Id', opp.id);
        SynQuotesToOpp ctr = new SynQuotesToOpp(new ApexPages.StandardController(opp));
        
        ctr.selectedQId = ctr.currentQuotes[0].id;
        ctr.SyncFromOpp();
    }
}