public class QuoteContact{   
	public static void updateQuoteContact(List<PBSI__Quote__c> records){
        //filter records with opp
        Set<ID> setOppID = new Set<ID>();
        List<PBSI__Quote__c> listQuote = new List<PBSI__Quote__c>();
        for(PBSI__Quote__c varQuote :records){
            if(varQuote.PBSI__Opportunity__c != null){
                setOppID.add(varQuote.PBSI__Opportunity__c);
                listQuote.add(varQuote);
            }
        }
        //
        Map<ID, ID> mapOppOcrId = OpportunityContactUtil.getSingleContactForOpps(setOppId);
        for(PBSI__Quote__c varQuote :listQuote){
            if(mapOppOcrId.containsKey(varQuote.PBSI__Opportunity__c)){
                varQuote.PBSI__Contact__c = mapOppOcrId.get(varQuote.PBSI__Opportunity__c);   
            }
        }
    }
    /*
	public static void updateQuoteContact(list<id> quoteIds)
    {
    	List<PBSI__Quote__c> quoteList = new List<PBSI__Quote__c>();
        List<String> oppIDList =  new List<String>();
        List<OpportunityContactRole> opportunityContactRoleList = new List<OpportunityContactRole>();
        List<ID> contIDList =  new List<ID>();
        List<Contact> contactList = new List<Contact>();            
            
        for(PBSI__Quote__c q : [SELECT PBSI__Contact__c, PBSI__Opportunity__c FROM PBSI__Quote__c WHERE Id in :quoteIds])
        {
            quoteList.add(q);
            system.debug(q.PBSI__Opportunity__c);
            oppIDList.add(q.PBSI__Opportunity__c);
        }
            
        for(opportunityContactRole opp :[SELECT ContactID FROM OpportunityContactRole WHERE IsPrimary = true AND OpportunityID in :oppIDList])
        {
            opportunityContactRoleList.add(opp);
            contIDList.add(opp.ContactId);
        }
            
        for(Integer i = 0; i < quoteList.size(); i++)
        {
            quoteList[i].PBSI__Contact__c = contIDList[i];
        }
        update quoteList;
    }
*/
}