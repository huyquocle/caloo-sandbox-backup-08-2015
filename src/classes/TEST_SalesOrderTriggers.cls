@isTest
private class TEST_SalesOrderTriggers{
    static testMethod void testPopulateQuoteData() {
        Test.startTest();
        
        PBSI__Quote__c quote = new PBSI__Quote__c(Name='Test Quote', CurrencyIsoCode='GBP');
        insert quote;
        
        PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c(PBSI__Order_Date__c=System.today(), CurrencyIsoCode='GBP');
        so.PBSI__From_Quote__c    = quote.ID;
        insert so;
        
        PBSI__PBSI_Sales_Order__c so2 = new PBSI__PBSI_Sales_Order__c(PBSI__Order_Date__c=System.today(), CurrencyIsoCode='GBP');
        insert so2;
        
        so2.PBSI__From_Quote__c    = quote.ID;
        update so2;
        
        Test.stopTest();
    }
    
    static testMethod void testBeforeInsertSalesOrderTrigger()
    {
        Opportunity opp = new Opportunity(
        	Name = 'Test Opp',
            Has_Welfare_facilities__c = 'Yes',
            Is_Easy_Access__c = 'Yes',
            Is_site_plan_produced_by_Caloo__c = 'No',
            Are_colours_specified_on_Quote__c = 'No',
            StageName = 'Enquiry',
            CloseDate = Date.newInstance(2014 , 10 ,25)
        );
        
        insert opp;
        
        PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c(
        	PBSI__Opportunity__c = opp.ID
        );
        
        insert so;
        
        
    }
}