@isTest
public class CmdApprovalURL_Test {
    static testmethod void Test() {
        Contact c = new Contact(lastname='Test contact');
        insert c;
        
        Account a = new Account(name='Test acc', Location__c='Bristol', Category__c='Media');
        insert a;
        
        Opportunity o = new Opportunity(name='Test opp', Account=a, Contact__c=c.Id, stagename='HotList', closedate=date.parse('12/12/2014'), LeadSource='Other');
        insert o;
        
        Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(o.id);
        req1.setSubmitterId(UserInfo.getUserId());
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});
        
        req1.setProcessDefinitionNameOrId('Approve_Red_Opp');
        req1.setSkipEntryCriteria(true);
    
        // Submit the approval request for the cli
        Approval.ProcessResult result = Approval.process(req1);
        
        CmpApprovalURLController con = new CmpApprovalURLController();
        con.generateApprovalURL(o.id);
    }
}