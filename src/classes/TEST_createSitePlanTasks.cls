@isTest
public class TEST_createSitePlanTasks{
    private static testMethod void testTrigger(){
        PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c(PBSI__Order_Date__c=System.today(), CurrencyIsoCode='GBP');
        so.Is_site_plan_produced_by_Caloo__c	= 'Yes';
        so.Heras_Security_Fencing_needed__c		= 'Yes';
        insert so;
        
        PBSI__PBSI_Sales_Order__c so2 = new PBSI__PBSI_Sales_Order__c(PBSI__Order_Date__c=System.today(), CurrencyIsoCode='GBP');
        so2.Is_site_plan_produced_by_Caloo__c	= 'Yes';
        insert so2;
    }
}