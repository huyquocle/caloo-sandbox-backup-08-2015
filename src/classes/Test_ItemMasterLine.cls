@IsTest
public class Test_ItemMasterLine {
    private static testMethod void testClass()
    {
        PBSI__PBSI_Location__c location = new PBSI__PBSI_Location__c();
        location.name = 'test';
        insert location;
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c();
        itemGroup.Name = 'test';
        itemGroup.Is_Edit_Group__c = true;
        insert ItemGroup;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Item_Group__c = itemGroup.id;
        item.Name = 'test';
        item.PBSI__Cost__c = 100;
        item.PBSI__description__c = 'test description';
        item.PBSI__Default_Location__c = location.id;
        insert item;
        
        Account a = new Account(Name='testAccount');
	    insert a; 
        
        Opportunity opp = new Opportunity(Name='Test',PBSI__DeliveryAddress__c= 'test',
                                          Account = a, StageName = 'Hotlist',CloseDate=System.Date.today() );
        insert opp;
        
        PBSI__PBSI_Opportunity_Line__c oppLine = new PBSI__PBSI_Opportunity_Line__c();
        oppLine.PBSI__Item__c = item.id;
        oppLine.PBSI__Opportunity__c = opp.id;
        insert oppLine;
        
        
        Test.startTest();
        
        PageReference page = new PageReference('/apex/ItemMasterLine');
        Test.setCurrentPage(page);
        
        
        ApexPages.StandardController sc = new ApexPages.StandardController(opp);
        ItemMasterLineControlller controller = new ItemMasterLineControlller(sc);
        
        controller.Save();
        
        Test.stopTest();
        
        //oppLine. 
    }

}