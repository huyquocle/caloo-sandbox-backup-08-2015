public class TriggerUtils{

   /**
      Get email template Id from Developer name
   */
   public static String getEmailTemplateId(String name){
      List<EmailTemplate> templates = [Select Id from EmailTemplate where DeveloperName=:name limit 1];
      if(templates.size() > 0){
         return templates.get(0).Id;
      }
      return null;
   }
        
   /**
      Generates email notification to the selected recipient with selected template. It also allows to send email automatically.
   */
   public static Messaging.SingleEmailMessage generateEmail(Id theTemplate, Id theWhatId, Id theRecipient, boolean theIsSend){
      if (theRecipient == null){
         return null;
      }
      Messaging.SingleEmailMessage aMail = new Messaging.SingleEmailMessage();
      aMail.setTemplateId(theTemplate);
      aMail.setTargetObjectId(theRecipient);
      aMail.setWhatId(theWhatId);
      aMail.setSaveAsActivity(false);
      if (theIsSend && theTemplate != null){
         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {aMail});
      }
      return theTemplate == null ? null : aMail;
   }
}