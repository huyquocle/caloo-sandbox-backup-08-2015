@isTest
private class LeadUpdateTestClass
{
	static Lead objLead;
	
	private static void testData_forLeadUpdateTest()
	{
		// Create a PostCodes__c Record
		PostCodes__c objPostCode = new PostCodes__c();
		objPostCode.Name = 'B13';
		objPostCode.Sales_Area__c = '1';
		insert objPostCode;
		
		// Create another PostCodes__c Record
		PostCodes__c objnewPostCode = new PostCodes__c();
		objnewPostCode.Name = 'B14';
		objnewPostCode.Sales_Area__c = '2';
		insert objnewPostCode;	
		
		// Create a Lead Record
		objLead = new Lead();
		objLead.PostalCode = 'B13';
		objLead.LastName = 'Test LastName';
		objLead.Company = 'Test Company';
		objLead.Category__c = 'Other';
		objLead.CurrencyIsoCode = 'GBP';
		objLead.LeadSource = 'Other';
		objLead.Industry = 'Other';
		insert objLead;		
	}
	
	// Test Method for Lead on Insert
    static testMethod void leadUpdateTestforInsert()
    {
    	testData_forLeadUpdateTest();
    	
    	test.startTest();
        Lead testLead = [SELECT Postcode_Area__c, Sales_Area__c FROM Lead WHERE Id = :objLead.Id];
        System.assertEquals('1', testLead.Sales_Area__c);
        test.stopTest();
    }
    
    // Test Method for Lead on Update
    static testMethod void leadUpdateTestforUpdate()
    {
    	testData_forLeadUpdateTest();
    	
    	objLead.PostalCode = 'B14';
    	
    	test.startTest();
    	update objLead;
    	
    	Lead testLead = [SELECT Postcode_Area__c, Sales_Area__c FROM Lead WHERE Id = :objLead.Id];
        System.assertEquals('2', testLead.Sales_Area__c);
        test.stopTest();
    }
}