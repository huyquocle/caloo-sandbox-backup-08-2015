@isTest
private class Test_QuoteContact{
    static testMethod void validQuoteContact(){
        
        Contact c = new Contact(lastname='Test contact');
        insert c;
        
        Account a = new Account(name='Test acc', Location__c='Bristol', Category__c='Media');
        insert a;
        
        Opportunity o = new Opportunity(name='Test opp', Account=a, Contact__c=c.Id, stagename='HotList', closedate=date.parse('12/12/2014'), LeadSource='Other');
        insert o;
        
        OpportunityContactRole ocr = new OpportunityContactRole(ContactId = c.Id, OpportunityId = o.Id, IsPrimary = true);
        insert ocr;
        
        PBSI__Quote__c q = new PBSI__Quote__c(Name='Test quote', PBSI__Opportunity__c=o.id);        
        //System.debug('Quote before trigger: Name: ' + q.Name + ' | Contact: ' + q.PBSI__Contact__c + ' | Opportunity: ' + q.PBSI__Opportunity__c);
        
        insert q;
        
        q = [SELECT PBSI__Contact__c, PBSI__Opportunity__c, Name FROM PBSI__Quote__c WHERE Id = :q.Id];
        //System.debug('Quote after trigger: Name: ' + q.Name + ' | Contact: ' + q.PBSI__Contact__c + ' | Opportunity: ' + q.PBSI__Opportunity__c);
        
        System.assertEquals(q.PBSI__Contact__c, c.id);
    }
}