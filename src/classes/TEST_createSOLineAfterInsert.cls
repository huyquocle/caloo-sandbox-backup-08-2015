@isTest
private class TEST_createSOLineAfterInsert{
    private static testMethod void testTrigger(){
        
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c(Name='DUMMYDOUN',pbsi__Description__c='descr');
      	insert itemGroup;
    
     	PBSI__PBSI_Location__c loc=new PBSI__PBSI_Location__c(Name='asfafasf');  
     	insert loc;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c(Name='MON-LEWISHAM-PRELIM');
        item.CurrencyIsoCode	= 'GBP';
        item.PBSI__Default_Location__c	= loc.ID;
        item.PBSI__Item_Group__c		= itemGroup.ID;
        item.PBSI__description__c		= 'description';
        insert item;
    	
    	PBSI__PBSI_Sales_Order__c so = new PBSI__PBSI_Sales_Order__c(PBSI__Order_Date__c=System.today(), CurrencyIsoCode='GBP');
      	
      	Test.startTest();
      	insert so;
        
        Test.stopTest();
    }
}