@isTest
public class UpdateOppAfterUpdateTest {
    static testMethod void TestM() {
        Test.startTest();
        Opportunity opp = new Opportunity(Name='test opp', StageName='stage', Probability = 95, CloseDate=system.today());
        insert opp;
        
        PBSI__PBSI_Item_Group__c igr = new PBSI__PBSI_Item_Group__c();        
        igr.Margin__c = 100;
        insert igr;
        system.debug('igr='+ igr);
        PBSI__PBSI_Location__c lc = new PBSI__PBSI_Location__c();
        lc.Name='test l';
        insert lc;
        
        system.debug('igr='+ igr.Margin__c);
        PBSI__PBSI_Item__c it = new PBSI__PBSI_Item__c();
        it.PBSI__Item_Group__c = igr.id;
        it.PBSI__Item_Group__r = igr;
        it.PBSI__salesprice__c = 10;
        it.Name ='AAAA';
        it.PBSI__Default_Location__c =lc.id;
        it.PBSI__description__c ='Test';
        insert it;
        
        system.debug('it='+ it.PBSI__Item_Group__r.Margin__c);
        
        PBSI__PBSI_Opportunity_Line__c op = new PBSI__PBSI_Opportunity_Line__c();
        op.PBSI__Opportunity__c = opp.id;
        op.PBSI__Quantity__c =2;
        op.PBSI__Sale_Price__c = 100;
        op.Total_Cost__c = 2;
        op.PBSI__Discount__c = 1;
        op.PBSI__Item__c = it.id;
        insert op;
        system.debug('Last='+ op.PBSI__Item__r.PBSI__Item_Group__r.Margin__c);
        update op;
        
        Test.stopTest();
    }
    
}