public class QuoteLineController {
    public PBSI__Quote__c Quote {get;set;}
    public List< PBSI__Quote_Line__c> listQuoteLine {get;set;}
    public QuoteLineController(ApexPages.StandardController stdController)
    {
        Quote = (PBSI__Quote__c)stdController.getRecord();
        
        // string idquote = 'a1e1100000066gL';
        // Quote = [select Id,Name,Heading_Group__c  from PBSI__Quote__c where id=:idquote limit 1 ][0];
        queryQuoteLine();
    }
    
    public void queryQuoteLine(){
        listQuoteLine= [select Id, ItemName__c, Item_Group_Conga__c, PBSI__Description__c,PBSI__Sale_Price__c, Display_Order__c,Heading_Group__c ,PBSI__Item__c   from PBSI__Quote_Line__c where PBSI__Quote__c =:Quote.Id order by Display_Order__c ];
        
    }
    public void Save()
    {
        try
        {
            //PBSI__Quote__c.Heading_Group__c
            string[] headingGroups = Quote.Heading_Group__c.split(',', -1);
            Map<string, integer> map_group_sortOrder = new Map<string, integer>();
            
            for(integer i = 0; i < headingGroups.size(); i++){
                headingGroups[i] = headingGroups[i].Trim();
                map_group_sortOrder.put(headingGroups[i], i + 1);
            }
            
            Quote.Heading_Group__c = string.join(headingGroups, ',');
            update Quote;  
            
            for( PBSI__Quote_Line__c item : listQuoteLine )
            {
                if(map_group_sortOrder.containsKey(item.Heading_Group__c))
                    item.Display_Order__c = map_group_sortOrder.get(item.Heading_Group__c);
                else
                {
                    item.Display_Order__c = null;
                    item.Heading_Group__c = null;
                }}
            update listQuoteLine;
            //queryQuoteLine();
        }
        catch(Exception e)
        {
            apexpages.addmessage(new apexpages.message(apexpages.severity.error, e.getMessage()));
        }
    }
    public List<SelectOption> getlistNumber()
    {
        List<SelectOption> temp = new List<SelectOption>();
        for (integer i = 0; i<= 100; i++) {
            temp.add(new SelectOption(string.valueof(i), string.valueof(i)));
        }
        return temp;
    }
    public List<SelectOption> getlistHeadingGroup()
    {
        List<SelectOption> headingGroups = new List<SelectOption>();
        headingGroups.add(new SelectOption('', '--none--'));
        if( Quote.Heading_Group__c != null )
        {
            string[] M = Quote.Heading_Group__c.split(',', -1);
            for( string item: M)
            {
                headingGroups.add(new SelectOption(item.trim(), item.trim()));
            }
        }
        return headingGroups;
    }
    public boolean isUsingHeadingGroup {get;set;}
    public void UsingHeadingGroup()
    {
        boolean cbUsingGroupValue =  Boolean.valueOf( apexpages.currentpage().getparameters().get('cbUsingGroupValue'));
        if( cbUsingGroupValue == false)
            return;
        List<PBSI__Quote__c> ListOldHeading = [select Id,Name,Heading_Group__c  
                                               from PBSI__Quote__c   
                                               where createdbyid=:UserInfo.getUserId() and id !=:Quote.Id and Is_Empty_Heading__c = true 
                                               order by LastModifiedDate 
                                               desc limit 1 ];
        if( ListOldHeading.size() <1)
            return;
        Quote.Heading_Group__c = ListOldHeading[0].Heading_Group__c;
    }
}