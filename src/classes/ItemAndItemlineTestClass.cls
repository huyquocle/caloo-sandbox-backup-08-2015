@isTest 
private class ItemAndItemlineTestClass {
    static testMethod void testItem() {
        
         PBSI__PBSI_Location__c location = new PBSI__PBSI_Location__c();
        location.name = 'test';
        insert location;
        PBSI__PBSI_Item_Group__c itemGroup = new PBSI__PBSI_Item_Group__c();
        itemGroup.Name = 'test';
        itemGroup.Is_Edit_Group__c = true;
        insert ItemGroup;
        
        PBSI__PBSI_Item__c item = new PBSI__PBSI_Item__c();
        item.PBSI__Item_Group__c = itemGroup.id;
        item.Name = 'test';
        item.PBSI__Cost__c = 100;
        item.PBSI__description__c = 'test description';
        item.PBSI__Default_Location__c = location.id;
        insert item;
        
        PBSI__BOM__c bom = new PBSI__BOM__c();
        insert bom;
        
        PBSI__BOM_Line__c line = new PBSI__BOM_Line__c();
        line.PBSI__Item__c = item.id;
        line.PBSI__BOM__c = bom.id;
        insert line;
        update item;
    }
}