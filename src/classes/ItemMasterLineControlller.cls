public class ItemMasterLineControlller {
    public List<PBSI__PBSI_Opportunity_Line__c> listItemGroup {get;set;}
    public Opportunity Opp {get;set;}
    
    public string test {get;set;}
    
    public ItemMasterLineControlller(ApexPages.StandardController stdController)
    {
        Opp = (Opportunity)stdController.getRecord();
        listItemGroup =  [select Id,PBSI__Item__r.Name,PBSI__ItemDescription__c,PBSI__Item_Description_Long__c,PBSI__Item__r.PBSI__defaultunitofmeasure__c,PBSI__Quantity__c,PBSI__Discount__c, PBSI__Sale_Price__c,PBSI__Line_SubTotal__c, PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c,Item_Cost__c,Custom_Cost_Price__c
                          from PBSI__PBSI_Opportunity_Line__c where PBSI__Opportunity__c=: Opp.Id and PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c = true order by createddate asc];
    }
    public void Save()
    {
        try
        {
            update listItemGroup;
            listItemGroup =  [select Id,PBSI__Item__r.Name,PBSI__ItemDescription__c,PBSI__Item_Description_Long__c,PBSI__Item__r.PBSI__defaultunitofmeasure__c,PBSI__Quantity__c,PBSI__Discount__c, PBSI__Sale_Price__c,PBSI__Line_SubTotal__c, PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c,Item_Cost__c,Custom_Cost_Price__c
                              from PBSI__PBSI_Opportunity_Line__c where PBSI__Opportunity__c=: Opp.Id and PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c = true order by createddate asc];
        }
        catch(Exception e)
        {
            apexpages.addmessage(new apexpages.message(apexpages.severity.error, e.getMessage()));
        }
    }
}