public with sharing class SynQuotesToOpp {
    
    public id CurrentOppID {set;get;}    
    public string selectedQId {set;get;}
    public List<PBSI__Quote__c> currentQuotes {set;get;}
    public Opportunity CurrentOpp {set;get;}
    public List<PBSI__PBSI_Opportunity_Line__c> OppLines {get;set;}
    
    public SynQuotesToOpp(ApexPages.StandardController stdController) {
        CurrentOppID = ApexPages.CurrentPage().getParameters().get('Id');
        selectedQId = '';
        if (CurrentOppID != null)
        {
            List<Quote_Adjustment_Setting__c> settings = [SELECT Object_Name__c, Source_Field__c, Target_Field__c FROM Quote_Adjustment_Setting__c];
            
            Set<string> quoteFields = new Set<string>();
            Set<string> oppFields = new Set<string>();
            Set<string> oppLineFields = new Set<string>();
            
            quoteFields.add('Id');
            quoteFields.add('Name');
            quoteFields.add('PBSI__Status__c');
            quoteFields.add('Sales_Price_of_Product__c');
            quoteFields.add('PBSI__Total__c');
            quoteFields.add('Caloo_Project_Discount1__c');
            quoteFields.add('Caloo_Net_Quote_Total__c');
            quoteFields.add('Caloo_Quote_Total__c');
            
            oppFields.add('Id');
            oppFields.add('Name');
            
            oppLineFields.add('Id');
            oppLineFields.add('Name');
            
            for (Quote_Adjustment_Setting__c setting : settings) {
                if (setting.Object_Name__c == 'Opportunity') {
                    quoteFields.add(setting.Target_Field__c);
                    oppFields.add(setting.Source_Field__c);
                }
                else if (setting.Object_Name__c == 'OpportunityLine') {
                    oppLineFields.add(setting.Source_Field__c);
                }
            }
            
            string quoteFieldList = String.join(new List<string>(quoteFields), ',');
            string quoteQueryString = 'SELECT ' + quoteFieldList + ' FROM PBSI__Quote__c WHERE PBSI__Opportunity__c = \'' + CurrentOppID + '\'';
            this.currentQuotes = (List<PBSI__Quote__c>)(Database.query(quoteQueryString));
            
            string oppFieldList = String.join(new List<string>(oppFields), ',');
            string oppQueryString = 'SELECT ' + oppFieldList + ' FROM Opportunity WHERE Id = \'' + CurrentOppID + '\'';
            this.CurrentOpp = (Opportunity)(Database.query(oppQueryString));
            
            string oppLineFieldList = String.join(new List<string>(oppLineFields), ',');
            string oppLineQueryString = 'SELECT ' + oppLineFieldList + ' FROM PBSI__PBSI_Opportunity_Line__c WHERE PBSI__Opportunity__c = \'' + CurrentOppID + '\'';
            this.OppLines = (List<PBSI__PBSI_Opportunity_Line__c>)(Database.query(oppLineQueryString));
        }
    }
    
    public PageReference SyncFromOpp()
    {
        if (!selectedQId.isWhitespace())
        {
            PBSI__Quote__c selectedQuoteItem;
            for (PBSI__Quote__c currentQuote : currentQuotes) {
                if (currentQuote.id==selectedQId) { 
                    selectedQuoteItem = currentQuote.clone(true);
                }
                currentQuote.PBSI__Status__c='Inactive';                
            }
            update currentQuotes;
            
            //set value for Quote
            selectedQuoteItem.PBSI__Status__c='Active';
            //selectedQuoteItem.PBSI__Contact__c = this.CurrentOpp.OpportunityContactRoles[0].ContactId;
            //selectedQuoteItem.PBSI__Disable_Trade_Agreements__c = !this.CurrentOpp.PBSI__Enable_Trade_Agreements__c;
            
            List<Quote_Adjustment_Setting__c> settings = [SELECT Object_Name__c, Source_Field__c, Target_Field__c FROM Quote_Adjustment_Setting__c];
            for (Quote_Adjustment_Setting__c setting : settings) {
                if (setting.Object_Name__c == 'Opportunity') {
                    selectedQuoteItem.put(setting.Target_Field__c, this.CurrentOpp.get(setting.Source_Field__c));
                }
            }            
            update selectedQuoteItem;
            
            //Get Existed Quote lines
            List<PBSI__Quote_Line__c> ExistedQuotes = [SELECT id FROM PBSI__Quote_Line__c WHERE PBSI__Quote__c =:selectedQId];
            if (ExistedQuotes!=null && ExistedQuotes.size() > 0) {
                delete ExistedQuotes;
            }
            
            List<PBSI__Quote_Line__c> newQuoteLines = new List<PBSI__Quote_Line__c>();
            for (PBSI__PBSI_Opportunity_Line__c oppLine : this.OppLines) { 
                PBSI__Quote_Line__c quoteLine = new PBSI__Quote_Line__c();
                quoteLine.PBSI__From_Opp__c = true;
                quoteLine.PBSI__Quote__c = selectedQId;                
                for (Quote_Adjustment_Setting__c setting : settings) {
                    if (setting.Object_Name__c == 'OpportunityLine') {
                        quoteLine.put(setting.Target_Field__c, oppLine.get(setting.Source_Field__c));
                    }
                }
                newQuoteLines.add(quoteLine);
            }
            if (newQuoteLines.size() > 0) {
                insert newQuoteLines;
            }
        }        
        PageReference pageRef = new PageReference('/'+selectedQId);
        pageRef.setRedirect(true);       
        return pageRef ; 
    }   
}