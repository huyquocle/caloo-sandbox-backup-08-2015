global class Quote_UpdateDiscountBatch implements Database.Batchable<sObject> {
    
    global Quote_UpdateDiscountBatch() {
        
    }
    public String str_SOQL = 'SELECT Id, Caloo_Project_Discount1__c, PBSI__Opportunity__c,SiteName1__c FROM PBSI__Quote__c ';
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(str_SOQL);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        list<PBSI__Quote__c> quotes = (list<PBSI__Quote__c>)scope;
        
        Set<Id> oppIds = new Set<Id>();
                
        for (PBSI__Quote__c quote:quotes) { 
            oppIds.add(quote.PBSI__Opportunity__c);
        }
        
        Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, ProjectDiscount__c,Site_name_product_range__c FROM Opportunity WHERE ID IN :oppIds]);
        
        for (PBSI__Quote__c quote:quotes) { 
            Opportunity opp = oppMap.get(quote.PBSI__Opportunity__c);
            if (opp != NULL && quote.Caloo_Project_Discount1__c == NULL) {
            	quote.Caloo_Project_Discount1__c = opp.ProjectDiscount__c;
                quote.SiteName1__c = opp.Site_name_product_range__c;
            }
        }
        
        update quotes;
    }
    
    global void finish(Database.BatchableContext BC){
    	 
    }
}