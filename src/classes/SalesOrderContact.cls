public class SalesOrderContact{
	public static void updateSalesOrderContact(List<PBSI__PBSI_Sales_Order__c> records){
        //filter records with opp
        Set<ID> setOppID = new Set<ID>();
        List<PBSI__PBSI_Sales_Order__c> listSO = new List<PBSI__PBSI_Sales_Order__c>();
        for(PBSI__PBSI_Sales_Order__c varSO :records){
            if(varSO.PBSI__Opportunity__c != null){
                setOppID.add(varSO.PBSI__Opportunity__c);
                listSO.add(varSO);
            }
        }
        //
        Map<ID, ID> mapOppOcrId = OpportunityContactUtil.getSingleContactForOpps(setOppId);
        for(PBSI__PBSI_Sales_Order__c varSo :listSo){
            if(mapOppOcrId.containsKey(varSo.PBSI__Opportunity__c)){
                varSo.PBSI__Contact__c = mapOppOcrId.get(varSo.PBSI__Opportunity__c);   
            }
        }
    }
}