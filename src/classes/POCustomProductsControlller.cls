public class POCustomProductsControlller {
    
    public List<PBSI__PBSI_Purchase_Order_Line__c> POLines {get;set;}
    
    public PBSI__PBSI_Purchase_Order__c Model {get;set;}
    
    public POCustomProductsControlller(ApexPages.StandardController stdController) {
        Model = (PBSI__PBSI_Purchase_Order__c)stdController.getRecord();
        
        GetPOLines();
    }
    
    private void GetPOLines() {
        POLines =  [SELECT Id,PBSI__Item__r.Name,PBSI__ItemDescription__c,PBSI__Item_Description_Long__c,PBSI__Item__r.PBSI__defaultunitofmeasure__c,PBSI__Quantity_Ordered__c,
                    PBSI__Discount__c,PBSI__Price__c,PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c
                    FROM PBSI__PBSI_Purchase_Order_Line__c 
                    WHERE PBSI__Purchase_Order__c=: Model.Id and PBSI__Item__r.PBSI__Item_Group__r.Is_Edit_Group__c = true 
                    ORDER BY CreatedDate ASC];
    }
    
    public void Cmd_Save() {
        SavePoint sp = Database.setSavepoint();
        try {
            update POLines;
            GetPOLines();
        }
        catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getMessage()));
            
            Database.rollback(sp);
        }
    }
    
}