@isTest
public class Test_QuoteLineController {
    private static TestMethod void test() {
        PageReference pageRef = Page.QuoteLine;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getHeaders().put('Host','');
                
        PBSI__Quote__c quote = new PBSI__Quote__c(Name='Test Quote', CurrencyIsoCode='GBP', Heading_Group__c = 'temp 1,temp 2, temp 3');
        insert quote;
        
        PBSI__Quote_Line__c line = new PBSI__Quote_Line__c(PBSI__Quote__c = quote.Id, Heading_Group__c = 'temp 1', Display_Order__c = 1);
        insert line;
        
        QuoteLineController controller = new QuoteLineController(new ApexPages.StandardController(quote));
        controller.getlistNumber();
        controller.getlistHeadingGroup();
        controller.save();
    }
}