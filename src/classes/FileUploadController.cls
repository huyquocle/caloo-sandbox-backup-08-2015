public with sharing class FileUploadController {
    
    public ApexPages.StandardController controller;
    
    public FileUploadController(ApexPages.StandardController con)
    {
        controller = con;
    }
    
    public Document document {
        get {
            PBSI__PBSI_Item__c item = (PBSI__PBSI_Item__c)(controller.getRecord());
            
            if (document == null)
                document = new Document();
            document.ContentType = 'image/jpeg';  
            document.IsInternalUseOnly = false;
            document.IsPublic = true;
            
            if (item != null)
                document.Name = item.Name +' Photo';
            
            return document;
        }
        set;
    }
    
    public PageReference upload() {
        
        PBSI__PBSI_Item__c item = (PBSI__PBSI_Item__c)(controller.getRecord());
        
        id oid = System.currentPageReference().getParameters().get('oid');
        
        Id photoId = null;
        document.AuthorId = UserInfo.getUserId();
        PBSI__PBSI_Item__c p;
        
        Id itemPhotoFolderId = [SELECT Id FROM Folder 
                                   WHERE Type = 'Document' 
                                   AND Name = 'Item Photos'][0].Id;
        
        if (itemPhotoFolderId != null) 
            document.FolderId = itemPhotoFolderId;
        else
            document.FolderId = UserInfo.getUserId(); // put it in running user's folder
        
        try {
            insert document;
            photoId = document.Id;
            string url = 'http://' + ApexPages.currentPage().getHeaders().get('Host').replace('visual', 'content');
            item.PBSI__Photo_URL__c = url  + '/servlet/servlet.ImageServer?id='+photoID+'&oid='+oid;
                update item;
            
        } catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading file'));
            return null;
        } finally {
            document.body = null; // clears the viewstate
            document = new Document();
        }
        
        PageReference curPage = ApexPages.currentPage();
        curPage.getParameters().put('success','1');
        curPage.getParameters().put('id',item.Id);
        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'File uploaded successfully'));
        curPage.setRedirect(true);
        return curPage;
    }    
}